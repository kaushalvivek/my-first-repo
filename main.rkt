#lang racket
;; ring-area
;; description: Fucktion that takes two arguments (outer-radius & inner-radius) and returns the area of the ring.
;; signature: ring-area: positive? positive? -> positive?
;; examples: (ring area 3 3) -> 0
;;           (ring-area 3 1) -> 15.7
(define (ring-area outer-radius inner-radius)
    (* 3.14 (- (* outer-radius outer-radius) (* inner-radius inner-radius))))

;; list-with-length-atleast-two?
;; description: Fuction takes in a list and returns true or false accordingly if the list contains more than two elements. 
;; signature: list-with-length-atleast-two?: list? -> boolean?
;; examples: (list-with-length-atleast-two? '(1 2 3 4)) -> #t
;;           (list-with-length-atleast-two? '(2)) -> #f 
(define (list-with-length-at-least-two? list)
    (if(empty? (rest list))
       #f
       #t))

;; remove-second
;; description: Fuction takes in a list of length greater than 2 and returns a list without the second element.
;; signature: list? -> list?
;; examples: (remove-second '(1 2 3 4)) -> '(1 3 4)
;;           (remove-second '(5 6 7)) -> '(5 7)  
(define (remove-second list)
    (if(list-with-length-at-least-two? list) (cons (first list) (rest (rest list)))
       (error "Enter a list greater than length 2.")))

;; list-length
;; description: Function takes in a list and returns it's length.
;; signature: list? -> numeric length?
;; examples: (list-length '(1 2 3 4 5)) -> 5
;;           (list-length '(4 2)) -> 2
(define (list-length list)
    (if(empty? list)
       0
       (+ 1 (list-length (rest list)))))

;; gcd
;; description: Function takes in 2 numbers and returns their gcd.
;; signature: positive? positive? -> positive?
;; examples: (gcd 1 4) -> 4
;;           (gcd 40 24) -> 8
(define (gcd x y)
    (if (= y 0)
        x
        (gcd y (remainder x y))))

(provide ring-area gcd list-with-length-at-least-two? list-length remove-second)